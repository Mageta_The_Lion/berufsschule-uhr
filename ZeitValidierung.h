//
// Created by Steven, Giulio & Angelo
//

#ifndef UHR_ZEITVALIDIERUNG_H
#define UHR_ZEITVALIDIERUNG_H
namespace ZeitValidierung {

    bool legalTime(int);

    bool legalHours(int);

    bool legalMinutes(int);

    bool legalSeconds(int);

    const int MAX_SECONDS_IN_DAY = 86400;
    const int SECONDS_IN_HOUR = 3600;
    const int SECONDS_IN_MINUTE = 60;
}


#endif //UHR_ZEITVALIDIERUNG_H
