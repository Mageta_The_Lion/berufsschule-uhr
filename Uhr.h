//
// Created by Steven, Giulio & Angelo
//

#ifndef UHR_UHR_H
#define UHR_UHR_H


#include <ctime>
#include "Zeit.h"

class Uhr {
private:

    std::time_t timeReferenceInSeconds = std::time(nullptr);
    int timeInSeconds = 0;
    int alarmTimeInSeconds = -1;
    int alarmDurationInSeconds = 10;
    bool alarmIsTriggerd = false;
    std::string alarmMessage = "Unpleasant Noise";
    static int removeDaysFromTimeInSeconds(int timeInSeconds);
    void updateTime();
    Zeit getZeit();
    static std::string generateTimeString(const Zeit& zeit);

public:

    explicit Uhr(int timeInSeconds);
    Uhr(int hour, int minute, int second);
    explicit Uhr(const Zeit& zeit);
    virtual ~Uhr();
    std::string getStringTimeIn12();
    std::string getStringTimeIn24();
    std::string getStringTimeIn12WithAlarm();
    std::string getStringTimeIn24WithAlarm();
    int getHourValue();
    int getMinutesValue();
    int getSecondsValue();
    void setTime(int timeInSeconds);
    void setTime(int hour, int minute, int second);
    void setAlarmTime(int timeInSeconds);
    void setAlarmTime(int hour, int minute, int second);
    void setAlarmToOff();
    void setAlarmMessage(const std::string&);
    void alarmTriggerdStatusUpdate();
    void addHours(int hours);
    void addMinutes(int minutes);
    void addSeconds(int seconds);
    int getAlarmDurationInSeconds() const;

    void setAlarmDurationInSeconds(int alarmDurationInSeconds);

};


#endif //UHR_UHR_H
