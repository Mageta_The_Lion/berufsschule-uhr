//
// Created by Steven, Giulio & Angelo
//

#include <stdexcept>
#include <iostream>
#include <iomanip>
#include "Uhr.h"
#include "ZeitValidierung.h"

Uhr::Uhr(int timeInSeconds) {
    if(ZeitValidierung::legalTime(timeInSeconds)){
        throw std::invalid_argument("Illegale timeInSeconds als Parameter im Konstruktor der Klasse Uhr.");
    }
    this->timeInSeconds = timeInSeconds;
}

Uhr::Uhr(int hour, int minute, int second) {

    if( !ZeitValidierung::legalHours(hour)){
        throw std::invalid_argument("Illegaler Parameter hour im Konstruktor der Klasse. Mögliche Werte: 0 - 23");
    }
    if(!ZeitValidierung::legalMinutes(minute)){
        throw std::invalid_argument("Illegaler Parameter minute im Konstruktor der Klasse. Mögliche Werte: 0 - 59");
    }
    if(!ZeitValidierung::legalSeconds(second)){
        throw std::invalid_argument("Illegaler Parameter second im Konstruktor der Klasse. Mögliche Werte: 0 - 59");
    }
    int timeInSeconds = 0;
    timeInSeconds += second;
    timeInSeconds += minute * ZeitValidierung::SECONDS_IN_MINUTE;
    timeInSeconds += hour * ZeitValidierung::SECONDS_IN_HOUR;

    this->timeInSeconds = timeInSeconds;

}

Uhr::Uhr(const Zeit& zeit) {
    int timeInSeconds = 0;
    timeInSeconds += zeit.getSecond();
    timeInSeconds += zeit.getMinute() * ZeitValidierung::SECONDS_IN_MINUTE;
    timeInSeconds += zeit.getHour() * ZeitValidierung::SECONDS_IN_HOUR;

    this->timeInSeconds = timeInSeconds;
}

Uhr::~Uhr() = default;

std::string Uhr::generateTimeString(const Zeit& zeit){
    std::string erg;
    char fillChar = '0';
    char seperator = ':';
    if(zeit.getHour() < 10){
        erg += fillChar;
    }
    erg += std::to_string(zeit.getHour());
    erg += seperator;
    if(zeit.getMinute() < 10){
        erg += fillChar;
    }
    erg += std::to_string(zeit.getMinute());
    erg += seperator;
    if(zeit.getSecond() < 10){
        erg += fillChar;
    }
    erg += std::to_string(zeit.getSecond());
    return erg;
}

std::string Uhr::getStringTimeIn12() {
    Zeit zeit = this->getZeit();
    if(zeit.getHour() > 12){
        zeit.setHour(zeit.getHour() - 12);
    }
    return this->generateTimeString(zeit);
}

std::string Uhr::getStringTimeIn24() {
    Zeit zeit = this->getZeit();
    return this->generateTimeString(zeit);

}

int Uhr::getHourValue(){
    return this->getZeit().getHour();
}

int Uhr::getMinutesValue(){
    return this->getZeit().getMinute();
}

int Uhr::getSecondsValue(){
    return this->getZeit().getSecond();
}

void Uhr::setTime(int timeInSeconds) {
    if(timeInSeconds > ZeitValidierung::MAX_SECONDS_IN_DAY){
        throw std::invalid_argument("Illegale timeInsSeconds als Parameter in der Methode Uhr::setTime()");
    }
    this->timeInSeconds = timeInSeconds;
}

void Uhr::setTime(int hour, int minute, int second) {
    if(!ZeitValidierung::legalHours(hour)){
        throw std::invalid_argument("Illegaler Parameter hour im Konstruktor des Objekts Zeit. Range (0-23)"); //
    }
    if(!ZeitValidierung::legalMinutes(minute)){
        throw std::invalid_argument("Illegaler Parameter minute im Konstruktor des Objekts Zeit. Range (0-59)"); //
    }
    if(!ZeitValidierung::legalSeconds(second)){
        throw std::invalid_argument("Illegaler Parameter second im Konstruktor des Objekts Zeit. Range (0-59)"); //
    }
    int timeToSet = 0;
    timeToSet += hour * ZeitValidierung::SECONDS_IN_HOUR;
    timeToSet += minute * ZeitValidierung::SECONDS_IN_MINUTE;
    timeToSet += second;
    this->setTime(timeToSet);
}

void Uhr::setAlarmTime(int timeInSeconds) {
    if(!ZeitValidierung::legalTime(timeInSeconds)){
        throw std::invalid_argument("Illegale Zeit als Parameter in der Methode Uhr::setAlarmTime");
    }
    this->alarmTimeInSeconds = timeInSeconds;
    this->alarmIsTriggerd = false;
}

void Uhr::setAlarmTime(int hour, int minute, int second) {
    if(!ZeitValidierung::legalHours(hour)){
        throw std::invalid_argument("Illegaler Parameter hour in der Methode Uhr::setAlarmTime. Range (0-23)");
    }
    if(!ZeitValidierung::legalMinutes(minute)){
        throw std::invalid_argument("Illegaler Parameter minute in der Methode Uhr::setAlarmTime. Range (0-59)");
    }
    if(!ZeitValidierung::legalSeconds(second)){
        throw std::invalid_argument("Illegaler Parameter second in der Methode Uhr::setAlarmTime. Range (0-59)");
    }
    int alarmToSet = 0;
    alarmToSet += hour * ZeitValidierung::SECONDS_IN_HOUR;
    alarmToSet += minute * ZeitValidierung::SECONDS_IN_MINUTE;
    alarmToSet += second;
    this->setAlarmTime(alarmToSet);
}

void Uhr::setAlarmMessage(const std::string& alarmMessage) {
    this->alarmMessage = alarmMessage;
}


int Uhr::removeDaysFromTimeInSeconds(int timeInSeconds) {
    return timeInSeconds % ZeitValidierung::MAX_SECONDS_IN_DAY;
}

void Uhr::updateTime() {
    int tempTimeStamp = static_cast<int>(std::time(nullptr));
    int timePassed = static_cast<int>(tempTimeStamp - timeReferenceInSeconds);
    this->timeInSeconds += timePassed;
    this->timeInSeconds = this->removeDaysFromTimeInSeconds(this->timeInSeconds);
    this->timeReferenceInSeconds = tempTimeStamp;

}

Zeit Uhr::getZeit() {
    this->updateTime();
    return Zeit(timeInSeconds);
}


void Uhr::alarmTriggerdStatusUpdate() {
    if(this->alarmTimeInSeconds >= 0){
        this->updateTime();
        Zeit alarmTime = Zeit(this->alarmTimeInSeconds);
        Zeit timeNow = this->getZeit();
        int alarmTimeAroundInSeconds = this->alarmTimeInSeconds + this->alarmDurationInSeconds - ZeitValidierung::MAX_SECONDS_IN_DAY;
        int alarmOffTimeInSecondes = this->alarmTimeInSeconds + this->alarmDurationInSeconds;

        if( this->timeInSeconds > alarmOffTimeInSecondes  || (this->timeInSeconds > alarmTimeAroundInSeconds && this->timeInSeconds < this->alarmTimeInSeconds) ){
            this->alarmIsTriggerd = false;
        }

        if(timeNow == alarmTime){
            this->alarmIsTriggerd = true;
        }

    }

}

std::string Uhr::getStringTimeIn12WithAlarm() {
    std::string erg = this->getStringTimeIn12();
    this->alarmTriggerdStatusUpdate();
    if(this->alarmIsTriggerd){
        erg += '\n';
        erg += this->alarmMessage;
    }
    return erg;
}

std::string Uhr::getStringTimeIn24WithAlarm() {
    std::string erg = this->getStringTimeIn24();
    this->alarmTriggerdStatusUpdate();
    if(this->alarmIsTriggerd){
        erg += '\n';
        erg += this->alarmMessage;
    }
    return erg;
}

void Uhr::setAlarmToOff() {
    this->alarmTimeInSeconds = -1;
    this->alarmIsTriggerd = false;

}

int Uhr::getAlarmDurationInSeconds() const {
    return alarmDurationInSeconds;
}

void Uhr::setAlarmDurationInSeconds(int alarmDurationInSeconds) {
    if(!ZeitValidierung::legalTime(alarmDurationInSeconds)){
        throw std::invalid_argument("Illegaler Parameter in der Methode Uhr::setAlarmDurationInSeconds.");
    }
    Uhr::alarmDurationInSeconds = alarmDurationInSeconds;
}

void Uhr::addHours(int hours) {
    if(hours < 0){
        throw std::invalid_argument("Illegaler Parameter in der Methode Uhr::addHour. Parameter hours muss positiv sein.");
    }
    int seconds = hours * ZeitValidierung::SECONDS_IN_HOUR;
    this->addSeconds(seconds);

}

void Uhr::addMinutes(int minutes) {
    if(minutes < 0){
        throw std::invalid_argument("Illegaler Parameter in der Methode Uhr::addMinutes. Parameter minutes muss positiv sein.");
    }
    int seconds = minutes * ZeitValidierung::SECONDS_IN_MINUTE;
    this->addSeconds(seconds);
}

void Uhr::addSeconds(int seconds) {
    if(seconds < 0){
        throw std::invalid_argument("Illegaler Parameter in der Methode Uhr::addSeconds. Parameter seconds muss positiv sein.");
    }
    this->timeInSeconds += this->removeDaysFromTimeInSeconds(seconds);
}
