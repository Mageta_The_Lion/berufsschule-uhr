//
// Created by Steven, Giulio & Angelo
//

#include "ZeitValidierung.h"

bool ZeitValidierung::legalTime(int timeInSeconds) {
    return timeInSeconds < ZeitValidierung::MAX_SECONDS_IN_DAY && timeInSeconds >= 0;
}

bool ZeitValidierung::legalHours(int hours) {
    return hours < 24 && hours >= 0;
}

bool ZeitValidierung::legalMinutes(int minutes) {
    return minutes < 60 && minutes >= 0;
}

bool ZeitValidierung::legalSeconds(int seconds) {
    return seconds < 60 && seconds >= 0;
}
