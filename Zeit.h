//
// Created by Steven, Giulio & Angelo
//

#ifndef UHR_ZEIT_H
#define UHR_ZEIT_H


class Zeit {
private:
    int hour = 0;
    int minute = 0;
    int second = 0;
public:
    Zeit(int hour, int minute, int second);
    explicit Zeit(int timeInSeconds);

    virtual ~Zeit();

    bool operator==(const Zeit &rhs) const;

    bool operator!=(const Zeit &rhs) const;

    int getHour() const;

    void setHour(int hour);

    int getMinute() const;

    void setMinute(int minute);

    int getSecond() const;

    void setSecond(int second);
};



#endif //UHR_ZEIT_H
