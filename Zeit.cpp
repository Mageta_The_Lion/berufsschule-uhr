//
// Created by Steven, Giulio & Angelo
//

#include <stdexcept>
#include "Zeit.h"
#include "ZeitValidierung.h"

Zeit::Zeit(int hour, int minute, int second) : hour(hour), minute(minute), second(second) {
    if(!ZeitValidierung::legalHours(hour)){
        throw std::invalid_argument("Illegaler Parameter hour im Konstruktor des Objekts Zeit. Range (0-23)"); //
    }
    if(!ZeitValidierung::legalMinutes(minute)){
        throw std::invalid_argument("Illegaler Parameter minute im Konstruktor des Objekts Zeit. Range (0-59)"); //
    }
    if(!ZeitValidierung::legalSeconds(second)){
        throw std::invalid_argument("Illegaler Parameter second im Konstruktor des Objekts Zeit. Range (0-59)"); //
    }
}

Zeit::Zeit(int timeInSeconds) {
    if(!ZeitValidierung::legalTime(timeInSeconds)){
        throw std::invalid_argument("Illegaler Parameter im Konstruktor des Objekts Zeit.");
    }
    int hours = timeInSeconds / ZeitValidierung::SECONDS_IN_HOUR;
    int minutes = (timeInSeconds - (hours * ZeitValidierung::SECONDS_IN_HOUR)) / ZeitValidierung::SECONDS_IN_MINUTE;
    int seconds = timeInSeconds - (hours * ZeitValidierung::SECONDS_IN_HOUR) - (minutes * ZeitValidierung::SECONDS_IN_MINUTE);
    this->setHour(hours);
    this->setMinute(minutes);
    this->setSecond(seconds);
}

Zeit::~Zeit() = default;

int Zeit::getHour() const {
    return hour;
}

void Zeit::setHour(int hour) {
    if(!ZeitValidierung::legalHours(hour)){
        throw std::invalid_argument("Der Parameter hour in Zeit::setHour ist nicht in Range(0-23).");
    }
    this->hour = hour;
}

int Zeit::getMinute() const {

    return this->minute;
}

void Zeit::setMinute(int minute) {
    if(!ZeitValidierung::legalMinutes(minute)){
        throw std::invalid_argument("Der Parameter minute in Zeit::setMinute ist nicht in Range(0-59).");
    }

    this->minute = minute;
}

int Zeit::getSecond() const {
    return this->second;
}

void Zeit::setSecond(int second) {

    if(!ZeitValidierung::legalSeconds(second)){
        throw std::invalid_argument("Der Parameter second in Zeit::setSecond ist nicht in Range(0-59).");
    }
    this->second = second;
}

bool Zeit::operator==(const Zeit &rhs) const {
    return hour == rhs.hour &&
           minute == rhs.minute &&
           second == rhs.second;
}

bool Zeit::operator!=(const Zeit &rhs) const {
    return !(rhs == *this);
}
